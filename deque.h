#pragma once
#include <iterator>
#include <algorithm>
#include <iostream>
#include <cstring>

template<typename T>
class Deque;

template<typename T>
class access_iterator {
public:
    access_iterator() {
        countElem = 0;
        _sizeBuff = 4;
        _buffer = new T[4];
    }

    void operator=(const access_iterator &other) {
        _sizeBuff = other._sizeBuff;
        countElem = other.countElem;

        delete[] _buffer;

        _buffer = new T[_sizeBuff];
        memcpy(_buffer, other._buffer, sizeof(T) * countElem);
    }

    ~access_iterator() {
        delete[] _buffer;
    }

    size_t size() const {
        return countElem;
    }

    bool empty() const {
        return countElem == 0;
    }

    void operator--() {
        countElem--;
        Balancing();
    }

    void operator--(int){
        operator--();
    }

    void operator << (const T& val) {
        _buffer[countElem++] = val;
        Balancing();
    }

    void Balancing() {
        if (countElem == _sizeBuff || (_sizeBuff > 4 && _sizeBuff >= 4 * countElem)) {

            T* newBuf = new T[2 * countElem];
            memcpy(newBuf, _buffer, sizeof(T) * countElem);

            std::swap(newBuf, _buffer);
            _sizeBuff = 2 * countElem;

            delete[] newBuf;
        }
    }

    T& operator*() {
        return _buffer[countElem - 1];
    }

    const T& operator*() const {
        return _buffer[countElem - 1];
    }

    T& operator[](size_t n) {
        return _buffer[n];
    }

    const T& operator[](size_t n) const {
        return _buffer[n];
    }

    void reverse() {
        for (int i = 0; i < (int)countElem / 2; i++)
            std::swap(_buffer[i], _buffer[countElem - i - 1]);
    }

private:
    T* _buffer;
    size_t countElem, _sizeBuff;

    friend class Deque<T>;
};

template<class Deque, class T>
class iteratorBase : public std::iterator<std::random_access_iterator_tag, T> {
public:

    iteratorBase(Deque *d, size_t index): index(index), deque_ptr(d) {
    }
    iteratorBase() {
        deque_ptr = NULL;
        index = 0;
    }

    iteratorBase(const iteratorBase &other) {
        index = other.index;
        deque_ptr = other.deque_ptr;
    }

    T* operator->() {
        return &((*deque_ptr)[index]);
    }

    T& operator*() {
        return (*deque_ptr)[index];
    }

    T& operator[](int n) {
        return (*deque_ptr)[index + n];
    }

    iteratorBase& operator++() {
        index++;
        return (*this);
    }

    iteratorBase& operator++(int) {
        iteratorBase bi(*this);
        index++;
        return bi;
    }

    iteratorBase& operator--() {
        index--;
        return (*this);
    }

    iteratorBase& operator--(int) {
        iteratorBase bi(*this);
        index--;
        return bi;
    }

    iteratorBase& operator+=(size_t n) {
        index += n;
        return (*this);
    }

    iteratorBase& operator-=(size_t n) {
        index -= n;
        return (*this);
    }


    iteratorBase operator+(const size_t n) const {
        iteratorBase temp(*this);
        temp += n;
        return temp;
    }

    iteratorBase operator-(const size_t n) const {
        iteratorBase temp(*this);
        temp -= n;
        return temp;
    }

    int operator-(const iteratorBase &other) const {
        return index - other.index;
    }

    int operator+(const iteratorBase &other) const {
        return index + other.index;
    }

    bool operator==(const iteratorBase &other) const {
        return (index == other.index && deque_ptr == other.deque_ptr);
    }

    bool operator<(const iteratorBase &other) const {
        return (index < other.index);
    }

    bool operator!=(const iteratorBase &other) const {
        return !((*this) == other);
    }

    bool operator<=(const iteratorBase &other) const {
        return (index <= other.index);
    }

private:
    size_t index;
    Deque* deque_ptr;
};

template<typename T>
class Deque {
public:
    typedef iteratorBase<Deque, T> iterator;
    typedef iteratorBase<const Deque, const T> const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    Deque<T>() {}
    Deque(const Deque&);
    ~Deque() = default;

    void push_back(const T& elem);
    void pop_back();
    void push_front(const T& elem);
    void pop_front();

    const T& back() const; //need to check. Are you sure that here is T, but not T&
    const T& front() const;//need to check. Are you sure that here is T, but not T&
    T& back();
    T& front();

    T& operator[] (const size_t index);
    const T& operator[] (const size_t index) const; //need to check. Are you sure that here is T, but not T&

    bool empty() const;
    size_t size() const;


    iterator begin();
    const_iterator begin() const;
    const_iterator cbegin() const;

    iterator end();
    const_iterator end() const;
    const_iterator cend() const;

    reverse_iterator rbegin();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator crbegin() const;

    reverse_iterator rend();
    const_reverse_iterator rend() const;
    const_reverse_iterator crend() const;

private:
    friend class iteratorBase<Deque, T>;

    access_iterator<T> _beginBuf, _endBuf;

    void Balancing() {
        size_t fullSize = _beginBuf.size() + _endBuf.size();

        if (_endBuf.size() > 2 * _beginBuf.size()) {
            _endBuf.reverse();
            _beginBuf.reverse();

            while (_beginBuf.size() < fullSize / 2) {

                _beginBuf << (*_endBuf);
                _endBuf--;
            }

            _endBuf.reverse();
            _beginBuf.reverse();
        }
        else
        if (_beginBuf.size() > 2 * _endBuf.size()) {

            _endBuf.reverse();
            _beginBuf.reverse();

            while (_endBuf.size() < fullSize / 2) {
                _endBuf << (*_beginBuf);
                _beginBuf--;
            }

            _endBuf.reverse();
            _beginBuf.reverse();
        }
    }
};

template<typename T>
Deque<T>::Deque(const Deque<T> &other) {
    _beginBuf = other._beginBuf;
    _endBuf = other._endBuf;
}

template<typename T>
T& Deque<T>::back() {
    if (_endBuf.size())
        return *_endBuf;
    else return *_beginBuf;
}

template<typename T>
T& Deque<T>::front() {
    if (_beginBuf.size())
        return *_beginBuf;
    else return *_endBuf;
}

template<typename T>
const T& Deque<T>::back() const {
    if (_endBuf.size())
        return *_endBuf;
    else return *_beginBuf;
}

template<typename T>
const T& Deque<T>::front() const {
    if (_beginBuf.size())
        return *_beginBuf;
    else return *_endBuf;
}

template<typename T>
typename Deque<T>::iterator Deque<T>::begin() {
    return iterator(this, 0);
}

template<typename T>
typename Deque<T>::iterator Deque<T>::end() {
    return iterator(this, size());
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::cbegin() const {
    return const_iterator(this, 0);
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::cend() const {;
    return const_iterator(this, size());
}

template<typename T>
typename Deque<T>::reverse_iterator Deque<T>::rbegin() {
    return std::reverse_iterator<iterator>(end());
}

template<typename T>
typename Deque<T>::reverse_iterator Deque<T>::rend() {
    return std::reverse_iterator<iterator>(begin());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crbegin() const {
    return std::reverse_iterator<const_iterator>(cend());
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crend() const {
    return std::reverse_iterator<const_iterator>(cbegin());
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::begin() const {
    return cbegin();
}

template<typename T>
typename Deque<T>::const_iterator Deque<T>::end() const {
    return cend();
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rbegin() const {
    return crbegin();
}

template<typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rend() const {
    return crend();
}

template<typename T>
size_t Deque<T>::size() const {
    return _beginBuf.size() + _endBuf.size();
}

template<typename T>
bool Deque<T>::empty() const {
    return size() == 0;
}

template<typename T>
void Deque<T>::push_back(const T& val) {
    _endBuf << (val);
    Balancing();
}

template<typename T>
void Deque<T>::push_front(const T& val) {
    _beginBuf << (val);
    Balancing();
}

template<typename T>
void Deque<T>::pop_back() {
    if (_endBuf.size()) {
        _endBuf--;
    }
    else {
        _beginBuf--;
    }
    Balancing();
}

template<typename T>
void Deque<T>::pop_front() {
    if (_beginBuf.size()) {
        _beginBuf--;
    }
    else {
        _endBuf--;
    }
    Balancing();
}

template<typename T>
T& Deque<T>::operator[](const size_t n) {
    if (n < _beginBuf.size())
        return _beginBuf[_beginBuf.size() - n - 1];
    else return _endBuf[(n - _beginBuf.size())];
}

template<typename T>
const T& Deque<T>::operator[](const size_t n) const {
    if (n < _beginBuf.size())
        return _beginBuf[_beginBuf.size() - n - 1];
    else return _endBuf[(n - _beginBuf.size())];
}
