#include <iostream>
#include "deque.h"
#include "GTests.h"

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}